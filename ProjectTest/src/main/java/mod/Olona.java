package mod;


import inter.FormAnnotation;
import inter.KeyAnnotation;
import inter.UrlMethod;
import models.ModelView;

import java.util.HashMap;

public class Olona {
//    attributes4
    @KeyAnnotation
    @FormAnnotation
    private int id;
    @KeyAnnotation
    @FormAnnotation
    private String olona;
//methodes
    public String getOlona() {
        return olona;
    }
    public void setOlona(String olona) {
        this.olona = olona;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
//eto ny methode mireturn modelview
    @UrlMethod(urlMethod = "olona-display")
    public ModelView display(){
        ModelView mod = new ModelView();
        mod.setAttribute("text");
        mod.setUrlPage("Test.jsp");
        mod.setValue("somme de 15+15=30");
        return mod;
    }
    @UrlMethod(urlMethod = "olona-info")
    public ModelView displayInfo(){
        ModelView mod = new ModelView();
        mod.setAttribute("text");
        mod.setUrlPage("Test.jsp");
        mod.setValue("id="+getId()+"<br> nom="+getOlona());
        return mod;
    }
}
